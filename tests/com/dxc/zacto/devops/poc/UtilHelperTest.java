package com.dxc.zacto.devops.poc;

import junit.framework.TestCase;

public class UtilHelperTest extends TestCase {
    UtilHelper helper = new UtilHelper();

    public void testConcatName() {
        assertEquals("Vamsi Rupanagudi", helper.concatName("Vamsi", "Rupanagudi"));
    }

    public void testSplitName() {
        String[] names = new String[2];
        names[0] = "Vamsi";
        names[1] = "Rupanagudi";
        String[] resultNames = helper.splitName("Vamsi Rupanagudi");
        assertEquals(2, resultNames.length);
        assertEquals(names[0], resultNames[0]);
        assertEquals(names[1], resultNames[1]);
    }

    public void testGetNumerologyNumber() {
        assertEquals(6, helper.getNumerologyNumber("ABC"));
        assertEquals(1, helper.getNumerologyNumber("ABCD"));
        assertEquals(6, helper.getNumerologyNumber("abc"));
        assertEquals(1, helper.getNumerologyNumber("abcd"));
        assertEquals(6, helper.getNumerologyNumber("aBC"));
        assertEquals(1, helper.getNumerologyNumber("abCD"));
        assertEquals(1, helper.getNumerologyNumber("jjjjjjjjjj"));
    }

    public void testGetBirthDatesForZodiacSign(){
        assertEquals("November 22-December 21", helper.getBirthDatesForZodiacSign("Sagittarius"));
        assertEquals("December 22-January 19", helper.getBirthDatesForZodiacSign("Capricorn"));
        assertEquals("April 20-May 20", helper.getBirthDatesForZodiacSign("Taurus"));
        assertEquals("February 19-March 20", helper.getBirthDatesForZodiacSign("Pisces"));
        assertEquals("January 20-February 18", helper.getBirthDatesForZodiacSign("Aquarius"));
    }
}
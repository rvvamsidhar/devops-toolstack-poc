package com.dxc.zacto.devops.poc;

import java.util.Date;
import java.util.HashMap;

class UtilHelper {
    static HashMap<String, Integer> letterDigits = new HashMap<String, Integer>();
    public static HashMap<String, String> zodiacBirthDates = new HashMap<String, String>();
    static {
        letterDigits.put("a", 1);
        letterDigits.put("b", 2);
        letterDigits.put("c", 3);
        letterDigits.put("d", 4);
        letterDigits.put("e", 5);
        letterDigits.put("f", 6);
        letterDigits.put("g", 7);
        letterDigits.put("h", 8);
        letterDigits.put("i", 9);
        letterDigits.put("j", 10);
        letterDigits.put("k", 11);
        letterDigits.put("l", 12);
        letterDigits.put("m", 13);
        letterDigits.put("n", 14);
        letterDigits.put("o", 15);
        letterDigits.put("p", 16);
        letterDigits.put("q", 17);
        letterDigits.put("r", 18);
        letterDigits.put("s", 19);
        letterDigits.put("t", 20);
        letterDigits.put("u", 21);
        letterDigits.put("v", 22);
        letterDigits.put("w", 23);
        letterDigits.put("x", 24);
        letterDigits.put("y", 25);
        letterDigits.put("z", 26);

        zodiacBirthDates.put("Aries", "March 21-April 19");
        zodiacBirthDates.put("Taurus", "April 20-May 20");
        zodiacBirthDates.put("Gemini","May 21-June 20");
        zodiacBirthDates.put("Cancer","June 21-July 22");
        zodiacBirthDates.put("Leo","July 23-August 22");
        zodiacBirthDates.put("Virgo","August 23-September 22");
        zodiacBirthDates.put("Libra","September 23-October 22");
        zodiacBirthDates.put("Scorpio","October 23-November 21");
        zodiacBirthDates.put("Sagittarius","November 22-December 21");
        zodiacBirthDates.put("Capricorn","December 22-January 19");
        zodiacBirthDates.put("Aquarius","January 20-February 18");
        zodiacBirthDates.put("Pisces","February 19-March 20");
    }

    void printName(String name) {
        System.out.println("Printing the name passed: " + name);
    }

    String[] splitName(String fullName) {
        String[] names = new String[2];
        names[0] = fullName.substring(0, fullName.indexOf(' '));
        names[1] = fullName.substring(fullName.indexOf(' ') + 1);
        return names;
    }

    String concatName(String firstname, String lastname) {
        return firstname + " " + lastname;
    }

    int getNumerologyNumber(String fullname) {
        int result = 0;
        try {
            fullname = fullname.trim();
            fullname = fullname.toLowerCase();
            final char[] nameChars = fullname.toCharArray();
            int number = 0;
            for (int i = 0; i < nameChars.length; i++) {
                if (nameChars[i] >= 97 && nameChars[i] <= 118 + 26) {
                    number += letterDigits.get(Character.toString(nameChars[i]));
                    System.out.println("Character: " + nameChars[i] + " " + "Number: " + letterDigits.get(Character.toString(nameChars[i])));
                }
            }
            System.out.println("Total for all characters: " + number);
            String numberString = new Integer(number).toString();
            int finalNumber = 0;
            int index = 0;
            char[] chars = numberString.toCharArray();
            if (chars.length == 1) finalNumber = number;
            while (chars.length > 1) {
                finalNumber = 0;
                for (int j = 0; j < chars.length; j++) {
                    finalNumber += new Integer(Character.toString(chars[j]));
                }
                numberString = new Integer(finalNumber).toString();
                chars = numberString.toCharArray();
            }
            result = finalNumber;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    String getBirthDatesForZodiacSign(String zodiac){
        return zodiacBirthDates.get(zodiac);
    }

    void commandLineParameters(){
        System.out.println("Command Parameters");
    }
}

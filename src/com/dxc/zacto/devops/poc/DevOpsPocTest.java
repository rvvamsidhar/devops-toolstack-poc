package com.dxc.zacto.devops.poc;

import java.util.Map;

public class DevOpsPocTest {
    public static void main(String args[]){
        UtilHelper helper = new UtilHelper();
        String fullname = "Rupanagudi Venkata Vamsidhar";
        helper.printName(fullname);
        String [] names = new String[10];
        names[0] = "Kambadur Vasantha Lakshmi";
        names[1] = fullname;
        names[2] = "Rupanagudi Venkata Sai Raga Bhavana";
        names[3] = "Gondi Deepa Chakravarthy";
        String[] splitNames = helper.splitName(fullname);
        System.out.println("Name entered: " + fullname);
        System.out.println("First Name: " + splitNames[0]);
        System.out.println("Last Name: " + splitNames[1]);
        helper.commandLineParameters();
        for(int i=0; i<names.length; i++){
            if(names[i] != null) {
                System.out.println("Numerological number for " + names[i] + " is " + helper.getNumerologyNumber(names[i]));
            }
        }
        for (Map.Entry<String,String> entry : UtilHelper.zodiacBirthDates.entrySet())
            System.out.println("Birth Dates for Zodiac Sign " + entry.getKey() + " are " + UtilHelper.zodiacBirthDates.get(entry.getKey()));
    }
}
